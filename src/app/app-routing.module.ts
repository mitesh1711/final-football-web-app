import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ResultsComponent} from "./results/results.component";
import {LeagueTableComponent} from "./league-table/league-table.component";
import {HeadToHeadComponent} from "./head-to-head/head-to-head.component";

const routes: Routes = [
  {path: 'leagueTable', component: LeagueTableComponent},
  {path: 'results', component: ResultsComponent},
  {path: 'headToHead', component: HeadToHeadComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
