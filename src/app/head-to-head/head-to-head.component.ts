import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../general.service';
import { TeamData } from '../team-data';
import {Team} from "../team";
import {Fixture} from "../fixture";
import {FixtureData} from "../fixture-data";
import {forEach} from "@angular/router/src/utils/collection";
@Component({
  selector: 'app-head-to-head',
  templateUrl: './head-to-head.component.html',
  styleUrls: ['./head-to-head.component.css']
})
export class HeadToHeadComponent implements OnInit {

  constructor(private generalService : GeneralService) { }

  teams: TeamData;
  selectedTeam1: Team;
  selectedTeam2: Team;
  link1: string;
  link2 : string;
  link3: string;
  fixtures: Fixture;
  fixtureData: FixtureData[];
  ngOnInit() {
    this.loadTeams();
  }

  loadTeams(){
    return this.generalService.getTeams().subscribe(data =>{
      this.teams = data;
    })
  }
  mySelectHandler1(event: Team){
    this.selectedTeam1 = event;
    this.link1 = this.selectedTeam1._links.fixtures.href;
    this.loadOneFixture(this.link1);
  }
  mySelectHandler2(event: Team){
    this.selectedTeam2 = event;
    this.link2 = this.selectedTeam2._links.fixtures.href;
    this.loadOneFixture(this.link2);
  }
  loadOneFixture(link){
    this.generalService.getOneFixture(link).subscribe(data =>{
      console.log(data);
      this.fixtures = data;
      this.fixtureData = this.fixtures.fixtures;
    })
  }
  compare(team1: Team, team2: Team){
    for (let f of this.fixtureData){
      if(team1.name === f.homeTeamName){
        this.link3 = this.fixtures._links.self.href;
      }
    }

  }

}
