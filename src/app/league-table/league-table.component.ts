import {Component, OnInit} from '@angular/core';
import {GeneralService} from "../general.service";
import {LeagueTable} from "../league-table";
import {Standing} from "../standing";

@Component({
  selector: 'app-league-table',
  templateUrl: './league-table.component.html',
  styleUrls: ['./league-table.component.css']
})
export class LeagueTableComponent implements OnInit {

  constructor(private general: GeneralService) {
  }

  leagueTable: LeagueTable;
  selectedLeague: LeagueTable;
  stand: Standing;
  home: Performance;
  away: Performance;

  ngOnInit() {
    this.loadLeagueTable();
  }

  loadLeagueTable() {
    this.general.getLeagueTable().subscribe(data => {
      //console.log(data);
      this.leagueTable = data;
    })
  }

  // To select one standing
  onSelect(sl: Standing) {
    this.stand = sl;
    this.home = this.stand.home;
    this.away = this.stand.away;
    //console.log(this.home);
  }

}
