import {HeadToHeadTeam} from "./head-to-head-team";
import {HeadToHeadFixturesData} from "./head-to-head-fixtures-data";

export class HeadToHead {
  constructor(public awayTeamWins: number, public count: number, public draws: number, public fixtures: HeadToHeadFixturesData, public homeTeamWins: number,
              public lastAwayWinAwayTeam: HeadToHeadTeam, public lastHomeWinHomeTeam: HeadToHeadTeam, public lastWinAwayTeam: HeadToHeadTeam,
              public lastWinHomeTeam: HeadToHeadTeam, public timeFrameEnd: string, public timeFrameStart: string) {
  }
}
