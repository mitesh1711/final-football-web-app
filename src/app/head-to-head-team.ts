import {Result} from "./result";
import {Link} from "./link";

export class HeadToHeadTeam {
  constructor(public awayTeamName: string, public date: string, public homeTeamName: string, public matchday: number, public odds: any,
              public result: Result, public stauts: string, public _links: Link){}
}
