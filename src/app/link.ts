import {Href} from "./href";

export class Link {
  constructor(public self: Href, public competition: Href, public homeTeam: Href, public awayTeam: Href, public fixtures: Href) {
  }
}
