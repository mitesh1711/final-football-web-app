import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {LeagueTable} from "./league-table";
import {TeamData} from "./team-data";
import {Fixture} from "./fixture";

const httpOption = {
  headers: new HttpHeaders({'X-Auth-Token': '62121c4f783e487fbc0785830af63bf4'})
};

@Injectable()
export class GeneralService {

  constructor(private http: HttpClient) {
  }

  getLeagueTable(): Observable<LeagueTable> {
    var urlPrefix = "http://api.football-data.org/v1/competitions/445/leagueTable";
    let body = this.http.get<LeagueTable>(urlPrefix, httpOption);

    return body;
  }

  getTeams(): Observable<TeamData> {
    var urlPrefix = "http://api.football-data.org/v1/competitions/445/teams";
    let body = this.http.get<TeamData>(urlPrefix, httpOption);

    return body;
  }

  getFixtures(): Observable<Fixture> {
    var urlPrefix = "http://api.football-data.org/v1/competitions/445/fixtures";
    let body = this.http.get<Fixture>(urlPrefix, httpOption);

    return body;
  }

  getOneFixture(link: string): Observable<Fixture> {
    var urlPrefix = "http://api.football-data.org/v1/fixtures/";
    var urlSuffix = "/" + link;
    let body = this.http.get<Fixture>(link, httpOption);
    return body;
  }


}
