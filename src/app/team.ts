import {Link} from "./link";

export class Team {
  constructor(public _links: Link, public name: string, public code: string, public shortName: string, public squadMarketValue: string,
              public crestUrl: string) {
  }
}
