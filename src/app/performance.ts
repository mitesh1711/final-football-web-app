export class Performance {
  constructor(public goals: number, public goalsAgainst: number, public wins: number, public draws: number, public losses: number) {
  }
}
