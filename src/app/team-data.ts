import {Link} from "./link";
import {Team} from "./team";

export class TeamData {
  constructor(public _links: Link, public count: number, public teams: Team[]) {
  }
}
