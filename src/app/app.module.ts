import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";

import {AppComponent} from './app.component';
import {LeagueTableComponent} from './league-table/league-table.component';
import {GeneralService} from "./general.service";
import {ResultsComponent} from './results/results.component';
import {AppRoutingModule} from ".//app-routing.module";
import {RouterModule} from "@angular/router";
import {HeadToHeadComponent} from './head-to-head/head-to-head.component';


@NgModule({
  declarations: [
    AppComponent,
    LeagueTableComponent,
    ResultsComponent,
    HeadToHeadComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule],
  providers: [GeneralService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
