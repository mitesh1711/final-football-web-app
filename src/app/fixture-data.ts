import {Result} from "./result";
import {Link} from "./link";

export class FixtureData {
  constructor(public _links: Link, public date: string, public status: string, public matchday: number,
              public homeTeamName: string, public awayTeamName: string, public result: Result) {
  }
}
