import {HeadToHeadFixture} from "./head-to-head-fixture";
import {HeadToHead} from "./head-to-head";

export class HeadToHeadTop {
  constructor(public fixture: HeadToHeadFixture, public head2head: HeadToHead) {
  }
}
