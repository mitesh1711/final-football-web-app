import {Standing} from "./standing";
import {Link} from "./link";

export class LeagueTable {
  constructor(public _links: Link, public leagueCaption: string, public matchDay: number, public standing: Standing[]) {
  }
}
