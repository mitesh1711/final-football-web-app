import {FixtureData} from "./fixture-data";
import {Link} from "./link";

export class Fixture {
  constructor(public _links: Link, public count: number, public fixtures: FixtureData[]) {
  }
}
