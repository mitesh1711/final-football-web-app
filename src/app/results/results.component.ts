import {Component, OnInit} from '@angular/core';
import {GeneralService} from "../general.service";
import {TeamData} from "../team-data";
import {Fixture} from "../fixture";
import {Team} from "../team";
import {FixtureData} from "../fixture-data";
import {HeadToHeadTop} from "../head-to-head-top";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  constructor(private generalService: GeneralService) {
  }

  teams: TeamData;
  fixtures: Fixture;
  selectedTeam: Team;
  link: string;
  fixtureData: FixtureData;
  headToHead: HeadToHeadTop;

  ngOnInit() {
    this.loadTeams();
    this.loadFixtures();
  }

  loadTeams() {
    this.generalService.getTeams().subscribe(data => {
      this.teams = data;
    })
  }

  loadFixtures() {
    this.generalService.getFixtures().subscribe(data => {

    })
  }

  onSelect(team: Team) {
    this.selectedTeam = team;
    this.link = this.selectedTeam._links.fixtures.href;
    //console.log(this.link);
  }

  loadOneFixture(link) {
    this.generalService.getOneFixture(link).subscribe(data => {
      //console.log(data);
      return this.fixtures = data;
    })
  }

  mySelectHandler(event: Team) {
    this.selectedTeam = event;
    this.link = this.selectedTeam._links.fixtures.href;
    //console.log(this.selectedTeam);
  }

  lastGameRecord(link) {
    this.generalService.getOneFixture(link).subscribe(data => {
      console.log(data);
      this.fixtures = data;
      this.fixtureData = this.fixtures.fixtures[this.fixtures.count - 1];
      //console.log(this.fixtureData);
    })
  }

  loadHeadToHead(link) {
    this.generalService.getOneFixture(link).subscribe(data => {
      //console.log(data);
      this.headToHead = data;
      //console.log(this.headToHead);
    })
  }
}
